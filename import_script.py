import psycopg2
import requests
import os

from config import COLUMNS, connect_to_db


def download_file():
    url = 'https://s3.amazonaws.com/nyc-tlc/trip+data/yellow_tripdata_2020-01.csv'
    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        print('Downloading the file...')
        with open('yellow_tripdata_2020-01.csv', 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192):
                f.write(chunk)


def create_database():
    command = 'CREATE DATABASE tripdata'
    conn = None
    try:
        conn = connect_to_db('postgres')
        conn.autocommit = True
        cur = conn.cursor()
        cur.execute(command)
        cur.close()
        print("Database created successfully...")
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def create_table():
    command = f'CREATE TABLE yellow_tripdata ({" ".join(COLUMNS)})'
    conn = None
    try:
        conn = connect_to_db()
        cur = conn.cursor()
        cur.execute(command)
        cur.execute(
            'CREATE INDEX idx_yellow_tripdata ON yellow_tripdata(tpep_pickup_datetime, tip_amount, total_amount);')
        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def add_data():
    conn = None
    try:
        conn = connect_to_db()
        cur = conn.cursor()
        with open('yellow_tripdata_2020-01.csv', 'r') as f:
            next(f)  # Skip the header row.
            cur.copy_from(f, 'yellow_tripdata', sep=',', null='')

        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    if not os.path.isfile('yellow_tripdata_2020-01.csv'):
        filename = download_file()
    create_database()
    create_table()
    add_data()
