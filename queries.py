import psycopg2

from config import connect_to_db


def execute_query(q):
    conn = None
    try:
        conn = connect_to_db()
        cur = conn.cursor()
        cur.execute(q)
        rows = cur.fetchone()
        return rows[0]
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def count_trips(total_amount=5):
    q = f'SELECT count(*) from yellow_tripdata WHERE total_amount < {total_amount}'
    result = execute_query(q)
    print("Count: ", result)
    return result


def most_profitable_hour():
    q = '''
        SELECT (EXTRACT(HOUR from tpep_pickup_datetime)) AS hour, SUM(total_amount) AS total_amount__sum FROM
        yellow_tripdata GROUP BY (EXTRACT(HOUR from tpep_pickup_datetime)) ORDER BY total_amount__sum DESC LIMIT 1
    '''
    result = execute_query(q)
    print("Hour: ", result)


def average_tip_percentage():
    q = '''
        SELECT ((AVG(tip_amount) / AVG(total_amount)) * 100) AS avg_total_amount_percent FROM yellow_tripdata
    '''
    result = execute_query(q)
    print("Percentage: ", result)



if __name__ == '__main__':
    count_trips()
    most_profitable_hour()
    average_tip_percentage()
