import psycopg2
import logging

from time_logger import MyLoggingConnection

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


DB_CONFIG = {
    "host": "localhost",
    "user": "dev",
    "password": "1234",
}

COLUMNS = ['vendorid INTEGER,',
           'tpep_pickup_datetime TIMESTAMP,',
           'tpep_dropoff_datetime TIMESTAMP,',
           'passenger_count INTEGER,',
           'trip_distance NUMERIC(9,2),',
           'ratecodeid INTEGER,',
           'store_and_fwd_flag VARCHAR(100),',
           'pulocationid INTEGER,',
           'dolocationid INTEGER,',
           'payment_type INTEGER,',
           'fare_amount NUMERIC(9,2),',
           'extra NUMERIC(9,2),',
           'mta_tax NUMERIC(9,2),',
           'tip_amount NUMERIC(9,2),',
           'tolls_amount NUMERIC(9,2),',
           'improvement_surcharge NUMERIC(9,2),',
           'total_amount NUMERIC(9,2),',
           'congestion_surcharge NUMERIC(9,2)']


def connect_to_db(db='tripdata'):
    conn = psycopg2.connect(connection_factory=MyLoggingConnection, database=db, **DB_CONFIG)
    conn.initialize(logger)
    return conn