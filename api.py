from fastapi import FastAPI

from queries import count_trips

app = FastAPI()

@app.get('/trips/amount/{count}')
def get_trips_amount(count: int):
    return count_trips(count)