# Limehome Coding Challenge

### Dependencies:
1. Python
2. Virtualenv
3. PostgreSQL

### SetUp
1. Create virtual env: ```virtualenv env```
2. Activate the env: ```source env/bin/activate```
3. Install the requirements: ```pip install -r requirements.txt```

### Usage
- Change **DB_CONFIG** in ***config.py*** with corresponding user and pass for PostgreSQL 
- Run ```python import_script.py``` to download and load the data in the database
- Run ```python queries.py``` to check the queries
- Run ```python import_ten_times_data.py``` to import the data 10 more times (it takes a while)
- Run ```uvicorn api:app --reload``` to load api server
- Endpoint link: [http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs)