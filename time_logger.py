import time
from psycopg2.extras import LoggingConnection, LoggingCursor


class MyLoggingCursor(LoggingCursor):
    def execute(self, query, vars=None):
        self.timestamp = time.time()
        return super(MyLoggingCursor, self).execute(query, vars)

    def callproc(self, procname, vars=None):
        self.timestamp = time.time()
        return super(MyLoggingCursor, self).callproc(procname, vars)

class MyLoggingConnection(LoggingConnection):
    def filter(self, msg, curs):
        return f'{msg} {int((time.time() - curs.timestamp) * 1000)} ms'

    def cursor(self, *args, **kwargs):
        kwargs.setdefault('cursor_factory', MyLoggingCursor)
        return LoggingConnection.cursor(self, *args, **kwargs)

